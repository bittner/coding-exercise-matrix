"""
Matrix validation.
"""

import csv

import numpy as np
import pandas as pd


def process(datafile):
    """
    Import the CSV data file and perform the validation steps.
    """
    reader = csv.reader(datafile)
    matrix = list(reader)

    if not matrix:
        msg = "Matrix is empty"
        raise ValueError(msg)

    try:
        numeric_matrix = np.array(matrix, dtype=float)
    except ValueError as err:
        error = str(err)
        if "inhomogeneous shape" in error:
            msg = "Not a valid matrix (irregular shape)"
            raise ValueError(msg) from err
        if "could not convert" in error:
            culprit = error.replace("could not convert ", "").replace(" to float", "")
            msg = f"Matrix element is not a number ({culprit})"
            raise ValueError(msg) from err
        raise

    series = pd.Series(numeric_matrix.flatten())
    duplicates = list(series[series.duplicated()])
    if duplicates:
        msg = f"There are duplicate values: {duplicates}"
        raise ValueError(msg)

    sums = np.sum(numeric_matrix, axis=0)
    all_sums_identical = sum(sums) / len(sums) == sums[0]
    if not all_sums_identical:
        msg = f"Not all columns have the same sum: {sums}"
        raise ValueError(msg)

    explain_success(numeric_matrix)


def explain_success(matrix):
    """
    Display validated matrix and explain validation criteria.
    """
    for row in matrix:
        print(row)

    print()
    print("This is a valid matrix:")
    print("✔ has at least one row")
    print("✔ contains only numbers, separated by commas")
    print("✔ all numbers are unique")
    print("✔ sum of each column is identical")

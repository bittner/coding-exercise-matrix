"""
Command line argument parsing and handling.
"""

import click

from . import validate


@click.command()
@click.version_option(package_name="matrix-validator")
@click.argument("datafile", type=click.File())
def main(datafile):
    """
    Read a CSV data file and trigger some content validation.
    """
    try:
        validate.process(datafile)
    except ValueError as err:
        msg = f"ERROR: {err}"
        raise SystemExit(msg) from err

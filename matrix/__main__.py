"""
Helper to run the CLI as a module (e.g. ``python3 -m matrix``).
"""

from sys import argv

from .cli import main

if __name__ == "__main__":
    main(argv[1:])

# Matrix Data File Validation

A CLI application that validates a CSV-style input file.

## Requirements

- A command line tool for validating an input file
- Validation rules:
  - Must contain one or more rows
  - Each row contains numbers separated by commas
  - There should not be any duplicate numbers within the file
  - The sum of all numbers in each column should be the same
- Program exit code:
  - 0 (VALID)
  - non-zero (INVALID), with error text on stdout

Implicit requirements:

- All rows must have the same number of columns, or empty columns
  count as zero (0).

## Why not just a simple script

- It's a very small overhead, we take a blueprint from the `cli-test-helpers`.
- Requirements grow, it rarely stays with what is defined in the beginning.
- We can add tests, properly.
- We can add a CI setup ("pipeline"), easily.
- Tests allow us to refactor the code, easily.
- Automatic test execution gives us transparency on code changes over time.

## Usage

```console
matrix-validator --help
```

## Installation

Install from the project's [PyPI registry](https://gitlab.com/bittner/coding-exercise-matrix/-/packages):
(see [GitLab docs](https://docs.gitlab.com/ee/user/packages/pypi_repository/#install-a-pypi-package))

```shell
PROJECT=bittner/coding-exercise-matrix
USER=<name-of-token>
TOKEN=<personal-access-token>
PIP_EXTRA_INDEX_URL=https://${USER}:${TOKEN}@gitlab.com/api/v4/projects/${PROJECT}/packages/pypi/simple
pip install matrix-validator
```

Install from source:

```console
pip install git+https://gitlab.com/bittner/coding-exercise-matrix#egg=matrix-validator
```

## Development

Clone this repository:

```console
git clone https://gitlab.com/bittner/coding-exercise-matrix
cd coding-exercise-matrix
```

Run from source, without installing: ("run as a module")

```console
python -m matrix --help
```

Install from source and use as an application: ("editable installation")

```console
python -m pip install -e .
matrix-validator --help
```

### Code Changes

After making code changes and adding tests, open a merge request. GitLab will
[mark test coverage results](https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html)
in the file diff view.

Use [Tox](https://tox.wiki/) to run linting and the tests locally, e.g.

```shell
tox list            # list all environments/jobs
tox                 # run all environments/jobs
tox -e ruff,py311   # run selected environments
tox -e py           # run tests with default Python
```

### Releases

To release a new version,
[update the package version](https://gitlab.com/bittner/coding-exercise-matrix/-/blob/main/pyproject.toml#L7)
and [create a new release with a new tag](https://gitlab.com/bittner/coding-exercise-matrix/-/releases)
matching the package version.

"""
Tests for the validate module.
"""

from pathlib import Path

import pytest

from matrix.validate import process


def fixture_files(filter: str) -> list:
    """Return a list of CSV files, either valid or invalid examples."""
    return list(Path("tests/fixtures").glob(f"{filter}-*.csv"))


@pytest.mark.parametrize(
    ("path", "name"),
    [(_, _.name) for _ in fixture_files("valid")],
)
def test_process_valid_succeeds(path, name):
    """
    Importing a valid CSV file succeeds.
    """
    with path.open() as csvfile:
        process(csvfile)


@pytest.mark.parametrize(
    ("path", "name"),
    [(_, _.name) for _ in fixture_files("invalid")],
)
def test_process_invalid_fails(path, name):
    """
    Importing an invalid CSV file fails.
    """
    with path.open() as csvfile, pytest.raises(ValueError):  # noqa: PT011
        process(csvfile)

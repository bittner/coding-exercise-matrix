"""
Tests for command line interface (CLI)
"""

from importlib import import_module
from importlib.metadata import version
from os import linesep
from unittest.mock import patch

import pytest
from cli_test_helpers import ArgvContext, shell
from click.testing import CliRunner

import matrix.cli


def test_main_module():
    """
    Exercise (most of) the code in the ``__main__`` module.
    """
    import_module("matrix.__main__")


def test_runas_module():
    """
    Can this package be run as a Python module?
    """
    result = shell("python -m matrix --help")
    assert result.exit_code == 0


def test_entrypoint():
    """
    Is entrypoint script installed? (``[project.scripts]``)
    """
    result = shell("matrix-validator --help")
    assert result.exit_code == 0


def test_version():
    """
    Does --version display information as expected?
    """
    expected_version = version("matrix-validator")
    result = shell("matrix-validator --version")

    assert result.stdout == f"matrix-validator, version {expected_version}{linesep}"
    assert result.exit_code == 0


def test_abort_without_filename():
    """
    Does CLI stop execution w/o a filename argument?
    """
    runner = CliRunner()
    result = runner.invoke(matrix.cli.main)

    assert result.exit_code != 0


def test_abort_with_nonexisting_file():
    """
    Does CLI stop execution w/ non-existing file?
    """
    runner = CliRunner()
    result = runner.invoke(matrix.cli.main, "this-file-does-not-exist.txt")

    assert result.exit_code != 0
    assert result.output.startswith("Usage:")


def test_display_error_for_invalid_file():
    """
    Do we display an error for an existing, but invalid file?
    """
    runner = CliRunner()
    result = runner.invoke(matrix.cli.main, "tests/fixtures/invalid-empty.csv")

    assert result.exit_code != 0
    assert result.output.startswith("ERROR:")


@patch("matrix.validate.process")
def test_invokes_process(mock_process):
    """
    Is the correct code called when invoked via the CLI?
    """
    test_argv = ["matrix-validator", "tests/fixtures/valid-minimal.csv"]

    with ArgvContext(*test_argv), pytest.raises(SystemExit):
        matrix.cli.main()

    assert mock_process.called
